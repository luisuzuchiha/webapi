<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="images/icon.ico" />
    <title>Bleach</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery-1.7.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/cufon-yui.js"></script>
    <script src="js/cufon-replace.js"></script>
    <script src="js/Kozuka_L_300.font.js"></script>
    <script src="js/Kozuka_B_700.font.js"></script>
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
   		<script type="text/javascript" src="js/html5.js"></script>
    	<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
	<![endif]-->
</head>
<body>
  <!--==============================header=================================-->
    <header>
    	<div class="main">
        	<h1><a href="Inicio.html"><img src="images/logo.jpg" alt=""></a></h1>
            <div class="social-icons">
                <span>Siguenos:</span>
                <a href="#" class="icon-3"></a>
                <a href="https://www.facebook.com/Anime-Otaku-521090357964594/" class="icon-2"></a>
                <a href="#" class="icon-1"></a>
            </div>
            <div class="clear"></div>
        </div>
    </header>  
    <nav>  
        <ul class="menu">
            <li><a href="Inicio.html">Inicio</a></li>
            <!--<li><a href="breeding.html">Breeding</a></li>
            <li><a href="events.html">Events</a></li> -->
            <li><a href="listaanime.php">Lista Anime</a></li>
            <li><a href="gallery.html">Galeria</a></li>
            <li><a href="../login.php">Salir</a></li>
        </ul>
        <div class="clear"></div>
     </nav>
  <!--==============================content================================-->
    <section id="content"><div class="ic"></div>
       <div class="container_12 top-4">
        <div class="grid_4">
        	<!--<h2>Contactanos</h2>
            <div class="map">
              <iframe src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Brooklyn,+New+York,+NY,+United+States&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=61.282355,146.513672&amp;ie=UTF8&amp;hq=&amp;hnear=Brooklyn,+Kings,+New+York&amp;ll=40.649974,-73.950005&amp;spn=0.01628,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
            </div>
            <dl class="adr">
                <dt>8901 Marmora Road, <br>Glasgow, D04 89GR.</dt>
                <dd><span>Telefono: </span>+57 3005488580<div class=""></div></dd>
                <dd><span>E-mail: </span><a href="https://outlook.live.com/owa/" class="link">luis-xv1994@hotmail.com</a></dd>
            </dl> -->
        </div>
        <div class="grid_8">
        	<div class="left-1">
            	<h2><font color= "#EC35D7">Bleach</font></h2>
                <form id="form" method="post" action="bleacht.php">
                  <fieldset>
                  <img src="images/anime/bleach.png" border="0" width="250" height="250" alt="League of Legends"><br/>
                  <span>Kurosaki Ichigo es un estudiante de instituto de 15 años, que tiene una peculiaridad: es capaz de ver, oír y hablar con fantasmas. Pero no sabe hasta dónde puede abarcar la clasificación de espíritus, ni lo que conlleva el saberlo. Un buen día, una extraña chica de pequeña estatura que viste ropas negras de samurai entra en su cuarto. Se llama Rukia Kuchiki, y es una Shinigami (Dios de la Muerte). Ante la incredulidad de Ichigo, le explica que su trabajo es mandar a las almas buenas o plus a un lugar llamado la Sociedad de Almas, y eliminar a las almas malignas o hollows. Luego junto a Inoue Orihime, Ishida Uryū y Sado Yasutora se veran envueltos en diferentes batallas, las cuales iran desarrollando sus diferentes habilidades que le otorgaran a cada uno su importancia en la serie.</span><br/><br/>
                  <p align="center">Titulo: Bleach</p> 
                  <p align="center">Otros títulos: Blanqueador – ブリーチ</p> 
                  <p align="center">Género: Comedia, Drama, Acción, Sobrenatural, Shounen</p>
                  <p align="center">Episodios: 366</p> 
                  <p align="center">Año: 2007 Audio: Japones </p>
                  <p align="center">Subtitulos: Español</p>
                  <p align="center">Formato: MP4 Duración: 24min. aprox.</p>
                  <p align="center">Peso: 99MB aprox.</p>
                  <p align="center">Resolución: 1280×720</p>
                  <p align="center">Uploader: LuisUzuchiha</p>
                  <h3><font color= "#EC35D7"><B>Capturas</B></font></h3><br/>
                  <img src="images/anime/bleachc.jpg" border="0" width="500" height="500" alt="League of Legends"><br/><br/>
                  <h3><font color= "#EC35D7"><B>Descargar</B></font></h3><br/>
                  <h3><font color= "#EC35D7"><B>1-100</B></font></h3><br/>
                  <a href="https://mega.nz/#F!jdl1GAKD!ebERwGC6mRIZ1sMlY7xldQ"><img src="images/mega2.png"></a><br/><br/>
                  <h3><font color= "#EC35D7"><B>101-200</B></font></h3><br/>
                  <a href="https://mega.nz/#F!nRtn3Yra!Gn7s01y4eAl6fi2KDV_bwg"><img src="images/mega2.png"></a><br/><br/>
                  <h3><font color= "#EC35D7"><B>201-300</B></font></h3><br/>
                  <a href="https://mega.nz/#F!nNtUGBqJ!NehtIzWuN28vJFwVVbxBig"><img src="images/mega2.png"></a><br/><br/>
                  <h3><font color= "#EC35D7"><B>301-366</B></font></h3><br/>
                  <a href="https://mega.nz/#F!fAd1nRga!LYvJxRKp9jxck28Zc-Gskg"><img src="images/mega2.png"></a><br/><br/>
<!--==============================Comentario=================================--> 
                  <h3><font color= "#EC35D7"><B>Escribe tu comentario</B></font></h3><br/>            
                  <textarea name="comentario"></textarea><p/>
                  <div class="btns"><a class="button" onClick="document.getElementById('form').submit()">Publicar</a></div><br/><br/>
                  <h3><font color= "#EC35D7"><B>Comentarios</B></font></h3><br/>
                  </fieldset>  
                </form> 
                <?php include("comentarios/bleach.html"); ?> 
            </div>
        </div>
        <div class="clear"></div>
       </div>
    </section> 
    <!--==============================footer=================================-->
  <footer>
      <div class="footer-col-1">
      <h3><font color= "#EC35D7">Admin y Uploader</font></h3>
          <h3>LuisUzuchiha</h3>
            <img src="images/luis.gif" border="1" width="190" height="100" alt="League of Legends">
        </div>
        <div class="footer-col-2">
          <h3><font color= "#EC35D7">Editor y Uploader</font></h3>
          <h3>Wallan21</h3>
          <img src="images/wallan21.jpg" border="1" width="190" height="100" alt="League of Legends">
        </div>  
        <div class="footer-col-3">
        <h3><font color= "#EC35D7">Colaboradores</font></h3>
        <h3>Juanki</h3>
          <img src="images/juanki.gif" border="1" width="190" height="100" alt="League of Legends" align="rigth">
        </div>   
        <div class="footer-col-4">
        <h3><font color= "#EC35D7">Disfruten :)</font></h3><br/>
        <img src="images/k.gif" border="1" width="200" height="115" alt="League of Legends" align="rigth">
        </div> 
        
  </footer>        
<script>
	Cufon.now();
</script>
</body>
</html>
