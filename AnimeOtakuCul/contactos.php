<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="images/icon.ico" />
    <title>Contactos</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery-1.7.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/cufon-yui.js"></script>
    <script src="js/cufon-replace.js"></script>
    <script src="js/Kozuka_L_300.font.js"></script>
    <script src="js/Kozuka_B_700.font.js"></script>
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
   		<script type="text/javascript" src="js/html5.js"></script>
    	<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
	<![endif]-->
</head>
<body>
  <!--==============================header=================================-->
    <header>
    	<div class="main">
        	<h1><a href="Inicio.html"><img src="images/logo.jpg" alt=""></a></h1>
            <div class="social-icons">
                <span>Siguenos:</span>
                <a href="#" class="icon-3"></a>
                <a href="https://www.facebook.com/Anime-Otaku-521090357964594/" class="icon-2"></a>
                <a href="#" class="icon-1"></a>
            </div>
            <div class="clear"></div>
        </div>
    </header>  
    <nav>  
        <ul class="menu">
            <li><a href="Inicio.html">Inicio</a></li>
            <li><a href="listaanime.php">Lista Anime</a></li>
            <li><a href="gallery.html">Galeria</a></li>
            <li class="current"><a href="contactos.php">Contactos</a></li>
        </ul>
        <div class="clear"></div>
     </nav>
  <!--==============================content================================-->
    <section id="content"><div class="ic"></div>
       <div class="container_12 top-4">
        <div class="grid_4">
        	<h2><font color= "#EC35D7">Contactanos</font></h2>
            <div class="map">
              <iframe src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Brooklyn,+New+York,+NY,+United+States&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=61.282355,146.513672&amp;ie=UTF8&amp;hq=&amp;hnear=Brooklyn,+Kings,+New+York&amp;ll=40.649974,-73.950005&amp;spn=0.01628,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
            </div>
            <dl class="adr">
                <dt>8901 Marmora Road, <br>Glasgow, D04 89GR.</dt>
                <dd><span>Telefono: </span>+57 3005488580<div class=""></div></dd>
                <dd><span>E-mail: </span><a href="https://outlook.live.com/owa/" class="link">luis-xv1994@hotmail.com</a></dd>
            </dl> 
        </div>
        <div class="grid_8">
        	<div class="left-1">
            	<h2><font color= "#EC35D7">Formulario</font></h2>
                <form id="form" method="post" >
                  <fieldset>
                    <label><strong>Nombre:</strong><input type="text" name="nombre" id="nombre" ></label>
                    <label><strong>C.C O TI:</strong><input type="text" name="CC" id="CC" ></label>
                    <label><strong>Email:</strong><input type="text" name="email" id="email" ></label>
                    <label><strong>Telefono:</strong><input type="text" name="telefono" id="telefono"></label>
                    <label><strong>Mensaje:</strong><textarea id="mensaje" name="mensaje"></textarea></label>
                    <div class="btns"><a href="contactos.php" class="button">Limpiar</a><a class="button" onClick="document.getElementById('form').submit()">Enviar</a></div>
                  </fieldset>  
                </form>
                <?php
// Realizo comexuon a la Base de Datos
include('conexion.php');

// Verifico si han sido enviadas variables por el metodo POST
if (isset($_POST['CC']))
{
    // Verifico que la empresa no este creada
    $mq_verificar=mysql_query("select * from mensajes where CC='".$_POST['CC']."'");
    $numero=mysql_num_rows($mq_verificar);
    if($numero>0)
    {
        ?><script language="JavaScript">alert("Este usuario ya Comento");</script><?php
    }
    else
    {        
         // Ingreso el registro a la tabla de la Base de Datos
         mysql_query("INSERT INTO mensajes(nombre,CC, email, Telefono, mensaje) VALUES ('{$_POST['nombre']}','{$_POST['CC']}', '{$_POST['email']}', '{$_POST['telefono']}', '{$_POST['mensaje']}')");              
    }
}
// Recupero y muestro los registro de la tabla Empresa
$mq_verificar2=mysql_query("select * from mensajes order by nombre asc");
$numerof=mysql_num_rows($mq_verificar2);
if ($numerof==0)
{
    ?><center><font color='red' face='Tahoma' size='3' >El sistema no encontro Mensajes Registrados...</font></center><?php
}
else
{
    ?><br>
    <table border="1" cellspacing="0" cellpadding="0" bordercolor="#D8CFFE" align="center" width="82%">
        <tr>
            <td colspan="7" align="center"><font color="red" face="Tahoma" size="3">Mensajes</font></td>
        </tr>
        <tr bgcolor="#DF01D7">
            <td width="10%"><font color="#01DF3A" face="Tahoma" size="2">   Nombre</font></td>
            <td width="16%"><font color="#01DF3A" face="Tahoma" size="2">   CC o TI</font></td>
            <td width="18%"><font color="#01DF3A" face="Tahoma" size="2">   Email</font></td>
            <td width="14%"><font color="#01DF3A" face="Tahoma" size="2">   Telefono</font></td>
            <td width="14%"><font color="#01DF3A" face="Tahoma" size="2">   Mensaje</font></td>
        </tr><?php
            while($fila2=mysql_fetch_array($mq_verificar2)) 
            { ?>
                <tr>
                    <td><font face='Tahoma' size='2'><?php echo"$fila2[0]"; ?></font></td>
                    <td><font face='Tahoma' size='2'><?php echo"$fila2[1]"; ?></font></td>
                    <td><font face='Tahoma' size='2'><?php echo"$fila2[2]"; ?></font></td>
                    <td><font face='Tahoma' size='2'><?php echo"$fila2[3]"; ?></font></td>
                    <td><font face='Tahoma' size='2'><?php echo"$fila2[4]"; ?></font></td>
                </tr>
            <?php } ?>
    </table>
    <br><?php   
}
mysql_close($db);
?> 
            </div>
        </div>
        <div class="clear"></div>
       </div>
    </section> 
<!--==============================footer=================================-->
 <footer>
      <div class="footer-col-1">
      <h3><font color= "#EC35D7">Admin y Uploader</font></h3>
          <h3>LuisUzuchiha</h3>
            <img src="images/luis.gif" border="1" width="190" height="100" alt="League of Legends">
        </div>
        <div class="footer-col-2">
          <h3><font color= "#EC35D7">Editor y Uploader</font></h3>
          <h3>Wallan21</h3>
          <img src="images/wallan21.jpg" border="1" width="190" height="100" alt="League of Legends">
        </div>  
        <div class="footer-col-3">
        <h3><font color= "#EC35D7">Colaboradores</font></h3>
        <h3>Juanki</h3>
          <img src="images/juanki.gif" border="1" width="190" height="100" alt="League of Legends" align="rigth">
        </div>   
        <div class="footer-col-4">
        <h3><font color= "#EC35D7">Disfruten :)</font></h3><br/>
        <img src="images/k.gif" border="1" width="200" height="115" alt="League of Legends" align="rigth">
        </div> 
        
  </footer>         
<script>
	Cufon.now();
</script>
</body>
</html>

