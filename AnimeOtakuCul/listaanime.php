<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="images/icon.ico" />
    <title>Lista Anime</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery-1.7.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/cufon-yui.js"></script>
    <script src="js/cufon-replace.js"></script>
    <script src="js/Kozuka_L_300.font.js"></script>
    <script src="js/Kozuka_B_700.font.js"></script>
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
   		<script type="text/javascript" src="js/html5.js"></script>
    	<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
	<![endif]-->
</head>
<body>
  <!--==============================header=================================-->
    <header>
    	<div class="main">
        	<h1><a href="Inicio.html"><img src="images/logo.jpg" alt=""></a></h1>
            <div class="social-icons">
                <span>Siguenos:</span>
                <a href="#" class="icon-3"></a>
                <a href="https://www.facebook.com/Anime-Otaku-521090357964594/" class="icon-2"></a>
                <a href="#" class="icon-1"></a>
            </div>
            <div class="clear"></div>
        </div>
    </header>  
    <nav>  
        <ul class="menu">
            <li><a href="Inicio.html">Inicio</a></li>
            <!--<li><a href="breeding.html">Breeding</a></li>
            <li><a href="events.html">Events</a></li> -->
            <li class="current"><a href="listaanime.php">Lista Anime</a></li>
            <li><a href="gallery.html">Galeria</a></li>
            <li><a href="../login.php">Salir</a></li>
        </ul>
        <div class="clear"></div>
     </nav>
  <!--==============================content================================-->
    <section id="content"><div class="ic"></div>
       <div class="container_12 top-4">
        <div class="grid_4">
        	<!--<h2>Contactanos</h2>
            <div class="map">
              <iframe src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Brooklyn,+New+York,+NY,+United+States&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=61.282355,146.513672&amp;ie=UTF8&amp;hq=&amp;hnear=Brooklyn,+Kings,+New+York&amp;ll=40.649974,-73.950005&amp;spn=0.01628,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
            </div>
            <dl class="adr">
                <dt>8901 Marmora Road, <br>Glasgow, D04 89GR.</dt>
                <dd><span>Telefono: </span>+57 3005488580<div class=""></div></dd>
                <dd><span>E-mail: </span><a href="https://outlook.live.com/owa/" class="link">luis-xv1994@hotmail.com</a></dd>
            </dl> -->
        </div>
        <div class="grid_8">
        	<div class="left-1">
            	<h2><font color= "#EC35D7">Lista Anime</font></h2>
                <form id="form" method="post" >
                  <fieldset>
                  <h2><font color= "#EC35D7">A</font></h2><br/>
                  <a href="angel.php"><img src="images/lista/angelb.jpg"></a><br/><br/>
                  <a href="another.php"><img src="images/lista/anothe.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">B</font></h2><br/>
                  <a href="bleach.php"><img src="images/lista/bleach.jpg"></a><br/><br/>
                  <a href="blood.php"><img src="images/lista/blood.jpg"></a><br/><br/>
                  <a href="bloodc.php"><img src="images/lista/bloodc.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">C</font></h2><br/>
                  <a href="clannad.php"><img src="images/lista/clannad.jpg"></a><br/><br/>
                  <a href="clannad2.php"><img src="images/lista/clannad2.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">D</font></h2><br/>
                  <a href="deathnote.php"><img src="images/lista/death.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">F</font></h2><br/>
                  <a href="fairy.php"><img src="images/lista/fairyt.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">G</font></h2><br/>
                  <a href="gintama.php"><img src="images/lista/gintama.jpg"></a><br/><br/>
                  <a href="guiltycrow.php"><img src="images/lista/gc.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">I</font></h2><br/>
                  <a href="inuyasha.php"><img src="images/lista/inuyasha.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">M</font></h2><br/>
                  <a href="magi.php"><img src="images/lista/magi.jpg"></a><br/><br/>
                  <a href="magi2.php"><img src="images/lista/magi2.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">N</font></h2><br/>
                  <a href="naruto.php"><img src="images/lista/naruto.jpg"></a><br/><br/>
                  <a href="narutos.php"><img src="images/lista/narutos.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">O</font></h2><br/>
                  <a href="onep.php"><img src="images/lista/onep.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">S</font></h2><br/>
                  <a href="shiki.php"><img src="images/lista/shiki.jpg"></a><br/><br/>
                  <a href="swordartonline.php"><img src="images/lista/sao.jpg"></a><br/><br/>
                  <a href="steansgate.php"><img src="images/lista/steang.jpg"></a><br/><br/>
                  <a href="strikerb.php"><img src="images/lista/strikeb.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">T</font></h2><br/>
                  <a href="tasogare.php"><img src="images/lista/tasogare.jpg"></a><br/><br/>
                  <a href="railingun.php"><img src="images/lista/railingun.jpg"></a><br/><br/>
                  <h2><font color= "#EC35D7">Z</font></h2><br/>
                  <a href="zero.php"><img src="images/lista/zero.jpg"></a><br/><br/><br/><br/>                    
                  </fieldset>  
                </form> 
            </div>
        </div>
        <div class="clear"></div>
       </div>
    </section> 
    <!--==============================footer=================================-->
  <footer>
      <div class="footer-col-1">
      <h3><font color= "#EC35D7">Admin y Uploader</font></h3>
          <h3>LuisUzuchiha</h3>
            <img src="images/luis.gif" border="1" width="190" height="100" alt="League of Legends">
        </div>
        <div class="footer-col-2">
          <h3><font color= "#EC35D7">Editor y Uploader</font></h3>
          <h3>Wallan21</h3>
          <img src="images/wallan21.jpg" border="1" width="190" height="100" alt="League of Legends">
        </div>  
        <div class="footer-col-3">
        <h3><font color= "#EC35D7">Colaboradores</font></h3>
        <h3>Juanki</h3>
          <img src="images/juanki.gif" border="1" width="190" height="100" alt="League of Legends" align="rigth">
        </div>   
        <div class="footer-col-4">
        <h3><font color= "#EC35D7">Disfruten :)</font></h3><br/>
        <img src="images/k.gif" border="1" width="200" height="115" alt="League of Legends" align="rigth">
        </div> 
        
  </footer>        
<script>
	Cufon.now();
</script>
</body>
</html>
