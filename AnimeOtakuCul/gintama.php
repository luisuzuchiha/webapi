<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="images/icon.ico" />
    <title>Gintama</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery-1.7.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/cufon-yui.js"></script>
    <script src="js/cufon-replace.js"></script>
    <script src="js/Kozuka_L_300.font.js"></script>
    <script src="js/Kozuka_B_700.font.js"></script>
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
   		<script type="text/javascript" src="js/html5.js"></script>
    	<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
	<![endif]-->
</head>
<body>
  <!--==============================header=================================-->
    <header>
    	<div class="main">
        	<h1><a href="Inicio.html"><img src="images/logo.jpg" alt=""></a></h1>
            <div class="social-icons">
                <span>Siguenos:</span>
                <a href="#" class="icon-3"></a>
                <a href="https://www.facebook.com/Anime-Otaku-521090357964594/" class="icon-2"></a>
                <a href="#" class="icon-1"></a>
            </div>
            <div class="clear"></div>
        </div>
    </header>  
    <nav>  
        <ul class="menu">
            <li><a href="Inicio.html">Inicio</a></li>
            <!--<li><a href="breeding.html">Breeding</a></li>
            <li><a href="events.html">Events</a></li> -->
            <li><a href="listaanime.php">Lista Anime</a></li>
            <li><a href="gallery.html">Galeria</a></li>
            <li><a href="../login.php">Salir</a></li>
        </ul>
        <div class="clear"></div>
     </nav>
  <!--==============================content================================-->
    <section id="content"><div class="ic"></div>
       <div class="container_12 top-4">
        <div class="grid_4">
        	<!--<h2>Contactanos</h2>
            <div class="map">
              <iframe src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Brooklyn,+New+York,+NY,+United+States&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=61.282355,146.513672&amp;ie=UTF8&amp;hq=&amp;hnear=Brooklyn,+Kings,+New+York&amp;ll=40.649974,-73.950005&amp;spn=0.01628,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
            </div>
            <dl class="adr">
                <dt>8901 Marmora Road, <br>Glasgow, D04 89GR.</dt>
                <dd><span>Telefono: </span>+57 3005488580<div class=""></div></dd>
                <dd><span>E-mail: </span><a href="https://outlook.live.com/owa/" class="link">luis-xv1994@hotmail.com</a></dd>
            </dl> -->
        </div>
        <div class="grid_8">
        	<div class="left-1">
            	<h2><font color= "#EC35D7">Gintama</font></h2>
                <form id="form" method="post" action="gintamat.php">
                  <fieldset>
                  <img src="images/anime/gintama.jpg" border="0" width="250" height="250" alt="League of Legends"><br/>
                  <span>La historia de Gintama nos sitúa en un Japón sometido por unas criaturas alienígenas conocidas como Amantos. Gracias a estos el país goza de avances tecnológicos desmedidos, pero a la vez la especie humana ha sido desprendida de la autoridad de velar sobre su propio planeta. Para evitar revuelas bélicas se ha prohibido por completo el uso y porte de espadas en cualquier circunstancia por parte de los terrícolas,pero aun así en esta nueva era hay personas que se mantienen fieles a sus creencias y convicciones antañas como es el caso del protagonista, Gintoki. Gin, junto con otros dos igualmente estrambóticos personajes crean una tienda conocida como la Yorozuya, en donde se dedican a realizar cualquier tipo de trabajo que se les presente, pero al final rara vez logran obtener algo de dinero decente y tienen problemas incluso para pagar el alquiler del piso.
</span><br/><br/>
                  <p align="center">Titulo: Gintama</p> 
                  <p align="center">Otros títulos: Silver Soul</p> 
                  <p align="center">Género: Acción, Comedia, Histórico, Samurai, Ciencia Ficción, Shounen</p>
                  <p align="center">Episodios: 201</p> 
                  <p align="center">Audio: Japones </p>
                  <p align="center">Subtitulos: Español</p>
                  <p align="center">Formato: MP4 Duración: 24min. aprox.</p>
                  <p align="center">Peso: 99MB aprox.</p>
                  <p align="center">Resolución: 1280×720</p>
                  <p align="center">Uploader: LuisUzuchiha</p>
                  <h3><font color= "#EC35D7"><B>Capturas</B></font></h3><br/>
                  <img src="images/anime/gintamac.jpg" border="0" width="500" height="500" alt="League of Legends"><br/><br/>
                  <h3><font color= "#EC35D7"><B>Descargar</B></font></h3><br/>
                  <h3><font color= "#EC35D7"><B>1-100</B></font></h3><br/>
                  <a href="https://mega.nz/#F!kUdQkRaT!o5ULEEWxJUV_SI4JjcERKQ"><img src="images/mega2.png"></a>
                  <h3><font color= "#EC35D7"><B>101-201</B></font></h3><br/>
                  <a href="https://mega.nz/#F!XVdElY4Q!OiSrXR590Kdi4taFNCwWZQ"><img src="images/mega2.png"></a><br/><br/>

<!--==============================Comentario=================================--> 
                  <h3><font color= "#EC35D7"><B>Escribe tu comentario</B></font></h3><br/>            
                  <textarea name="comentario"></textarea><p/>
                  <div class="btns"><a class="button" onClick="document.getElementById('form').submit()">Publicar</a></div><br/><br/>
                  <h3><font color= "#EC35D7"><B>Comentarios</B></font></h3><br/>
                  </fieldset>  
                </form> 
                <?php include("comentarios/gintama.html"); ?> 
            </div>
        </div>
        <div class="clear"></div>
       </div>
    </section> 
    <!--==============================footer=================================-->
  <footer>
      <div class="footer-col-1">
      <h3><font color= "#EC35D7">Admin y Uploader</font></h3>
          <h3>LuisUzuchiha</h3>
            <img src="images/luis.gif" border="1" width="190" height="100" alt="League of Legends">
        </div>
        <div class="footer-col-2">
          <h3><font color= "#EC35D7">Editor y Uploader</font></h3>
          <h3>Wallan21</h3>
          <img src="images/wallan21.jpg" border="1" width="190" height="100" alt="League of Legends">
        </div>  
        <div class="footer-col-3">
        <h3><font color= "#EC35D7">Colaboradores</font></h3>
        <h3>Juanki</h3>
          <img src="images/juanki.gif" border="1" width="190" height="100" alt="League of Legends" align="rigth">
        </div>   
        <div class="footer-col-4">
        <h3><font color= "#EC35D7">Disfruten :)</font></h3><br/>
        <img src="images/k.gif" border="1" width="200" height="115" alt="League of Legends" align="rigth">
        </div> 
        
  </footer>        
<script>
	Cufon.now();
</script>
</body>
</html>
